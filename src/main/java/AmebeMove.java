
import model.VehicleType;

/**
 *
 * @author ilya
 */
class AmebeMove {
    
    public VehicleType type;
    public int start;
    public int end;
    public Ceil from;
    public Ceil to;
    
    public AmebeMove(VehicleType type, int start, Ceil from, Ceil to) {
        this.type = type;
        this.start = start;
        this.from = from;
        this.to = to;
        this.end = start + speed();
    }
    
    public int speed()
    {
        switch (type) {
            case ARRV:
                return 200;
            case FIGHTER:
                return 60;
            case HELICOPTER:
                return 80;
            case IFV:
                return 160;
            case TANK:
                return 130;
            default:
                throw new AssertionError(type.name());
            
        }
    }
}
