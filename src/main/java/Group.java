
import java.util.HashMap;
import java.util.Map;
import model.*;


/**
 *
 * @author ilya
 */
public class Group {
    
    public int playerId;
    public final Map<Long, Vehicle> unitsById = new HashMap<>();
    public boolean deleted = false;
    
    public Group(long playerId) {
        this.playerId = (int) playerId;
    }
    
    public void add(Vehicle vehicle) {
        unitsById.put(vehicle.getId(), vehicle);
    }
    
    public void addAll(Map<Long, Vehicle> units) {
        unitsById.putAll(units);
    }
    
    public void delete() {
        deleted = true;
        unitsById.clear();
    }
    
}
