
import java.util.HashSet;
import java.util.Set;


/**
 *
 * @author ilya
 */
public class PathFinder {
    
    public boolean[][] graph = new boolean[3][3];
    
    public PathFinder()
    {
        
    }
    
    
    public Ceil getPath(Ceil from, Ceil to)
    {
        if (to.getDistanceTo(from) > 1) {
            graph[to.x][to.y] = false;
        }
        Set<Node> openList = new HashSet<>(16);
        Set<Integer> closedList = new HashSet<>(16);
        
        openList.add(new Node(from));
        while(!openList.isEmpty()) {
            
            Node current = openList.stream().min((Node o1, Node o2) -> {
                if (o1.getF() > o2.getF()) {
                    return 1;
                } else if (o1.getF() < o2.getF()) {
                    return -1;
                } else {
                    return 0;
                }
            }).orElse(null);
            
            if (current == null) {
                break;
            }
            
            closedList.add(current.code);
            openList.remove(current);
            
            if (current.equals(to)) {
                // path finded
                while (current.parent != null && !current.parent.equals(from)) {
                    current = current.parent;
                }
                return new Ceil(current.x, current.y);
            }
            
            Set<Node> neighbours = new HashSet<>(8);
            if (current.x > 0 && !graph[current.x-1][current.y]) {
                neighbours.add(new Node(current.x-1, current.y, current));
            }
            if (current.x < 2 && !graph[current.x+1][current.y]) {
                neighbours.add(new Node(current.x+1, current.y, current));
            }
            if (current.y > 0 && !graph[current.x][current.y-1]) {
                neighbours.add(new Node(current.x, current.y-1, current));
            }
            if (current.y < 2 && !graph[current.x][current.y+1]) {
                neighbours.add(new Node(current.x, current.y+1, current));
            }
            for (Node neighbour : neighbours) {
                if (closedList.contains(neighbour.code)) {
                    continue;
                }
                neighbour.d = neighbour.getDistanceTo(to);
                neighbour.e += current.e;
                openList.add(neighbour);
            }
        }
        return null;
    }
    
    private class Node extends Ceil {

        public Node parent;
        public double d = 0.0;
        public double e = 0.0;
        
        public Node(int x, int y, Node parent) {
            super(x, y);
            this.parent = parent;
        }
        
        public Node(Ceil ceil) {
            super(ceil.x, ceil.y);
            this.parent = null;
        }
        
        public double getF()
        {
            return d+e;
        }

    }
    
}
