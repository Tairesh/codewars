
import model.ActionType;




/**
 *
 * @author ilya
 */
public class Ceil {
    
    public static final double A = 72.0;
    public static final double d = 8.0;
    
    public int x;
    public int y;
    public double left;
    public double right;
    public double top;
    public double bottom;
    public double middleX;
    public double middleY;
    public int code;
    
    public Ceil(int x, int y) {
        this.x = x;
        this.y = y;
        this.left = d+x*A;
        this.right = d+(x+1)*A;
        this.top = d+y*A;
        this.bottom = d+(y+1)*A;
        this.middleX = left+A/2.0;
        this.middleY = top+A/2.0;
        this.code = x+y*100;
    }
    
    public boolean equals(Ceil ceil) {
        return this.x == ceil.x && this.y == ceil.y;
    }
    
    public double getDistanceTo(Ceil ceil) {
        return Math.hypot(ceil.x-x, ceil.y-y);
    }
    
}
