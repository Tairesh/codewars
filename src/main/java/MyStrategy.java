
import model.*;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Stream;

public final class MyStrategy implements Strategy {

    public Random random;

    public TerrainType[][] terrainTypeByCellXY;
    public WeatherType[][] weatherTypeByCellXY;

    public Player me;
    public Player enemy;
    public World world;
    public Game game;
    public Move move;

    private WorldObserver observer;

    public final Map<Long, Vehicle> unitById = new HashMap<>();
    public final Map<Long, Integer> updateTickByUnitId = new HashMap<>();
    public final Queue<Consumer<Move>> delayedMoves = new ArrayDeque<>();
//    public final List<Group> unitGroups = new ArrayList<>(20);

//    public boolean rush = false;
    public final Map<VehicleType, AmebeMove> amebeMoves = new HashMap<>();
    public static Map<VehicleType, Ceil> amebeToCeil = new HashMap<>();

    public double currentHamburgerDirection = Math.PI / 4;

    private boolean allPlaced = false;
    private int allPlacedTick = 0;
    private boolean fighterPlaced = false;
    private boolean fighterNeeds = true;
    private Map<VehicleType, Point2D> typeToCoords = new HashMap<>();
    private int workTick = 0;

    @Override
    public void move(Player me, World world, Game game, Move move) {

        initializeStrategy(world, game);
        initializeTick(me, world, game, move);
//        System.out.println(world.getTickIndex() + ": " + me.getRemainsingActionCooldownTicks());
        if (me.getRemainingActionCooldownTicks() > 0) {
            return;
        }

        if (executeDelayedMove()) {
            return;
        }

        move();

        executeDelayedMove();
    }

    /**
     * Инциализируем стратегию.
     *
     * Для этих целей обычно можно использовать конструктор, однако в данном
     * случае мы хотим инициализировать генератор случайных чисел значением,
     * полученным от симулятора игры.
     */
    private void initializeStrategy(World world, Game game) {
        if (random == null) {
            random = new Random(game.getRandomSeed());

            terrainTypeByCellXY = world.getTerrainByCellXY();
            weatherTypeByCellXY = world.getWeatherByCellXY();

            observer = WorldObserver.instance();

            amebeToCeil.put(VehicleType.TANK, new Ceil(2, 2));
            amebeToCeil.put(VehicleType.IFV, new Ceil(1, 1));
            amebeToCeil.put(VehicleType.ARRV, new Ceil(0, 0));
            amebeToCeil.put(VehicleType.HELICOPTER, new Ceil(1, 1));
            amebeToCeil.put(VehicleType.FIGHTER, new Ceil(2, 2));

        }
    }

    /**
     * Сохраняем все входные данные в полях класса для упрощения доступа к ним,
     * а также актуализируем сведения о каждой технике и времени последнего
     * изменения её состояния.
     */
    private void initializeTick(Player me, World world, Game game, Move move) {
        this.me = me;
        this.world = world;
        this.game = game;
        this.move = move;
        this.enemy = world.getOpponentPlayer();
        observer.observe(this);
        typeToCoords.put(VehicleType.FIGHTER, middleCoordinates(Ownership.ALLY, VehicleType.FIGHTER));
        typeToCoords.put(VehicleType.HELICOPTER, middleCoordinates(Ownership.ALLY, VehicleType.HELICOPTER));
        typeToCoords.put(VehicleType.TANK, middleCoordinates(Ownership.ALLY, VehicleType.TANK));
        typeToCoords.put(VehicleType.IFV, middleCoordinates(Ownership.ALLY, VehicleType.IFV));
        typeToCoords.put(VehicleType.ARRV, middleCoordinates(Ownership.ALLY, VehicleType.ARRV));
        if (allPlaced) {
        	workTick = world.getTickIndex() - allPlacedTick;
        }
    }

    /**
     * Достаём отложенное действие из очереди и выполняем его.
     *
     * @return Возвращает {@code true}, если и только если отложенное действие
     * было найдено и выполнено.
     */
    private boolean executeDelayedMove() {
        Consumer<Move> delayedMove = delayedMoves.poll();
        if (delayedMove == null) {
            return false;
        }

        delayedMove.accept(move);
        return true;
    }

    public Stream<Vehicle> streamVehicles(Ownership ownership, VehicleType vehicleType) {
        Stream<Vehicle> stream = unitById.values().stream();

        switch (ownership) {
            case ALLY:
                stream = stream.filter(vehicle -> vehicle.getPlayerId() == me.getId());
                break;
            case ENEMY:
                stream = stream.filter(vehicle -> vehicle.getPlayerId() != me.getId());
                break;
            default:
        }

        if (vehicleType != null) {
            stream = stream.filter(vehicle -> vehicle.getType() == vehicleType);
        }

        return stream;
    }

    public Stream<Vehicle> streamVehicles(Ownership ownership) {
        return streamVehicles(ownership, null);
    }

    public Stream<Vehicle> streamVehicles() {
        return streamVehicles(Ownership.ANY);
    }

    public Point2D middleCoordinates(Ownership ownership, VehicleType vehicleType) {
        double x = streamVehicles(ownership, vehicleType).mapToDouble(Vehicle::getX).average().orElse(Double.NaN);
        double y = streamVehicles(ownership, vehicleType).mapToDouble(Vehicle::getY).average().orElse(Double.NaN);
        return x == Double.NaN || y == Double.NaN ? null : new Point2D(x, y);
    }

    public enum Ownership {
        ANY,
        ALLY,
        ENEMY
    }

    private void tickZero() {
        for (VehicleType type : VehicleType.values()) {
            Point2D coords = typeToCoords.get(type);
            Ceil ceil = coords.toCeil();
            Ceil globalTarget = amebeToCeil.get(type);
            if (ceil.equals(globalTarget)) {
                continue;
            }

            PathFinder pathFinder = new PathFinder();
            if (type == VehicleType.HELICOPTER) {
                amebeMoves.values().stream().filter(move -> move.type == VehicleType.FIGHTER && move.end >= world.getTickIndex()).forEach(move -> {
                    pathFinder.graph[move.from.x][move.from.y] = true;
                    pathFinder.graph[move.to.x][move.to.y] = true;
                });
                Ceil f = typeToCoords.get(VehicleType.FIGHTER).toCeil();
                if (f.x < 3 && f.y < 3) {
                    pathFinder.graph[f.x][f.y] = true;
                }
            } else if (type == VehicleType.FIGHTER) {
                amebeMoves.values().stream().filter(move -> move.type == VehicleType.HELICOPTER && move.end >= world.getTickIndex()).forEach(move -> {
                    pathFinder.graph[move.from.x][move.from.y] = true;
                    pathFinder.graph[move.to.x][move.to.y] = true;
                });
                Ceil f = typeToCoords.get(VehicleType.HELICOPTER).toCeil();
                pathFinder.graph[f.x][f.y] = true;
            } else {
                amebeMoves.values().stream().filter(move -> move.type != type && move.type != VehicleType.HELICOPTER && move.type != VehicleType.FIGHTER && move.end >= world.getTickIndex()).forEach(move -> {
                    pathFinder.graph[move.from.x][move.from.y] = true;
                    pathFinder.graph[move.to.x][move.to.y] = true;
                });
                for (VehicleType t : VehicleType.values()) {
                    if (t != VehicleType.FIGHTER && t != VehicleType.HELICOPTER && t != type) {
                        Ceil f = typeToCoords.get(t).toCeil();
                        pathFinder.graph[f.x][f.y] = true;
                    }
                }
            }

            Ceil target = pathFinder.getPath(ceil, globalTarget);
            if (target != null) {
                select(type);
                moveTo(target.middleX - coords.x, target.middleY - coords.y);
                amebeMoves.put(type, new AmebeMove(type, world.getTickIndex(), ceil, target));
            }
        }
    }

    private void placeAmebes() {

    	// обработка завершённых движений
        amebeMoves.values().stream().filter(aMove -> aMove.end == world.getTickIndex()).forEach(aMove -> {
            Point2D coords = typeToCoords.get(aMove.type);
            Ceil ceil = coords.toCeil();
            Ceil globalTarget = amebeToCeil.get(aMove.type);
            boolean anyoneMove = streamVehicles(Ownership.ALLY, aMove.type).anyMatch(vehicle -> updateTickByUnitId.get(vehicle.getId()) == world.getTickIndex());
            if (anyoneMove) {
                amebeMoves.replace(aMove.type, new AmebeMove(aMove.type, aMove.start + 10, aMove.from, aMove.to));
            }
            if (ceil.equals(globalTarget) || anyoneMove) {
                return;
            }
            VehicleType type = aMove.type;
            PathFinder pathFinder = new PathFinder();
            if (type == VehicleType.HELICOPTER) {
                amebeMoves.values().stream().filter(move -> move.type == VehicleType.FIGHTER && move.end >= world.getTickIndex()).forEach(move -> {
                    pathFinder.graph[move.from.x][move.from.y] = true;
                    pathFinder.graph[move.to.x][move.to.y] = true;
                });
                Ceil f = typeToCoords.get(VehicleType.FIGHTER).toCeil();
                if (f.x < 3 && f.y < 3) {
                    pathFinder.graph[f.x][f.y] = true;
                }
            } else if (type == VehicleType.FIGHTER) {
                amebeMoves.values().stream().filter(move -> move.type == VehicleType.HELICOPTER && move.end >= world.getTickIndex()).forEach(move -> {
                    pathFinder.graph[move.from.x][move.from.y] = true;
                    pathFinder.graph[move.to.x][move.to.y] = true;
                });
                Ceil f = typeToCoords.get(VehicleType.HELICOPTER).toCeil();
                pathFinder.graph[f.x][f.y] = true;
            } else {
                amebeMoves.values().stream().filter(move -> move.type != type && move.type != VehicleType.HELICOPTER && move.type != VehicleType.FIGHTER && move.end >= world.getTickIndex()).forEach(move -> {
                    pathFinder.graph[move.from.x][move.from.y] = true;
                    pathFinder.graph[move.to.x][move.to.y] = true;
                });
                for (VehicleType t : VehicleType.values()) {
                    if (t != VehicleType.FIGHTER && t != VehicleType.HELICOPTER && t != type) {
                        Ceil f = typeToCoords.get(t).toCeil();
                        pathFinder.graph[f.x][f.y] = true;
                    }
                }
            }
            Ceil target = pathFinder.getPath(ceil, globalTarget);
            if (target != null) {

                select(aMove.type);
                moveTo(target.middleX - coords.x, target.middleY - coords.y);
                amebeMoves.replace(aMove.type, new AmebeMove(aMove.type, world.getTickIndex(), ceil, target));
            } else {
//                        amebeMoves.remove(aMove.type);
            }
        });
        VehicleType[] remove = amebeMoves.keySet().stream().filter((type) -> (amebeMoves.get(type).end <= world.getTickIndex())).toArray(i -> new VehicleType[i]);
        for (VehicleType rem : remove) {
            amebeMoves.remove(rem);
        }

        // обработка остановившихся амёб
        allPlaced = true;
        for (VehicleType type : VehicleType.values()) {
            if (type == VehicleType.FIGHTER && fighterPlaced) {
                continue;
            }
            Point2D coords = typeToCoords.get(type);
            Ceil ceil = coords.toCeil();
            Ceil globalTarget = amebeToCeil.get(type);
            boolean anyoneMove = streamVehicles(Ownership.ALLY, type).anyMatch(vehicle -> updateTickByUnitId.get(vehicle.getId()) == world.getTickIndex());
            if (ceil.equals(globalTarget) && !anyoneMove) {
                if (type == VehicleType.FIGHTER) {
                    fighterPlaced = true;
                    select(VehicleType.FIGHTER);
                    scale(coords.x, coords.y, 0.1);
                }
                continue;
            }
            allPlaced = false;
            if (amebeMoves.containsKey(type)) {
                continue;
            }

            PathFinder pathFinder = new PathFinder();
            if (type == VehicleType.HELICOPTER) {
                amebeMoves.values().stream().filter(move -> move.type == VehicleType.FIGHTER && move.end >= world.getTickIndex()).forEach(move -> {
                    pathFinder.graph[move.from.x][move.from.y] = true;
                    pathFinder.graph[move.to.x][move.to.y] = true;
                });
                Ceil f = typeToCoords.get(VehicleType.FIGHTER).toCeil();
                if (f.x < 3 && f.y < 3) {
                    pathFinder.graph[f.x][f.y] = true;
                }
            } else if (type == VehicleType.FIGHTER) {
                amebeMoves.values().stream().filter(move -> move.type == VehicleType.HELICOPTER && move.end >= world.getTickIndex()).forEach(move -> {
                    pathFinder.graph[move.from.x][move.from.y] = true;
                    pathFinder.graph[move.to.x][move.to.y] = true;
                });
                Ceil f = typeToCoords.get(VehicleType.HELICOPTER).toCeil();
                pathFinder.graph[f.x][f.y] = true;
            } else {
                amebeMoves.values().stream().filter(move -> move.type != type && move.type != VehicleType.HELICOPTER && move.type != VehicleType.FIGHTER && move.end >= world.getTickIndex()).forEach(move -> {
                    pathFinder.graph[move.from.x][move.from.y] = true;
                    pathFinder.graph[move.to.x][move.to.y] = true;
                });
                for (VehicleType t : VehicleType.values()) {
                    if (t != VehicleType.FIGHTER && t != VehicleType.HELICOPTER && t != type) {
                        Ceil f = typeToCoords.get(t).toCeil();
                        pathFinder.graph[f.x][f.y] = true;
                    }
                }
            }

            Ceil target = pathFinder.getPath(ceil, globalTarget);
            if (target != null) {

                select(type);
                moveTo(target.middleX - coords.x, target.middleY - coords.y);
                amebeMoves.put(type, new AmebeMove(type, world.getTickIndex(), ceil, target));
            }
        }
        if (allPlaced) {
            allPlacedTick = world.getTickIndex();
        }
    }

    private int step = 1;
    private int tickToNextStep = 0;

    private void buildHamburger() {
    	if (workTick < tickToNextStep) {
    		return;
    	}
        if (step == 1) {
            select(0, 0, 80, 44);
            delayedMoves.add(move -> {
                move.setAction(ActionType.ASSIGN);
                move.setGroup(1);
            });
            moveTo(62, 0);
            select(80, 80, 152, 116);
            delayedMoves.add(move -> {
                move.setAction(ActionType.ASSIGN);
                move.setGroup(3);
            });
            moveTo(62, 0);
            select(152, 152, 226, 190, VehicleType.TANK);
            delayedMoves.add(move -> {
                move.setAction(ActionType.ASSIGN);
                move.setGroup(5);
            });
            moveTo(62, 0);

            select(1);
            delayedMoves.add(move -> {
                move.setAction(ActionType.ADD_TO_SELECTION);
                move.setGroup(3);
            });
            delayedMoves.add(move -> {
                move.setAction(ActionType.ADD_TO_SELECTION);
                move.setGroup(5);
            });
            delayedMoves.add(move -> {
                move.setAction(ActionType.ASSIGN);
                move.setGroup(99);
            });
            select(0, 0, 1024, 1024);
            delayedMoves.add(move -> {
                move.setAction(ActionType.DESELECT);
                move.setGroup(99);
            });
            delayedMoves.add(move -> {
                move.setAction(ActionType.DESELECT);
                move.setBottom(1024);
                move.setRight(1024);
                move.setVehicleType(VehicleType.FIGHTER);
            });
            delayedMoves.add(move -> {
                move.setAction(ActionType.ASSIGN);
                move.setGroup(98);
            });
            step = 2;
            tickToNextStep = 190;

        } else if (step == 2) {
            select(1);
            moveTo(0, 30);
            select(3);
            moveTo(0, 30);
            select(5);
            moveTo(0, 30);
            step = 3;
            tickToNextStep = 290;
        } else if (step == 3) {
            select(0, 0, world.getWidth(), 90);
            delayedMoves.add(move -> {
                move.setAction(ActionType.ASSIGN);
                move.setGroup(10);
            });
            double x1 = streamVehicles(Ownership.ALLY).filter(vehicle -> vehicle.getY() < 90).mapToDouble(vehicle -> vehicle.getX()).average().orElse(Double.NaN);
            double y1 = streamVehicles(Ownership.ALLY).filter(vehicle -> vehicle.getY() < 90).mapToDouble(vehicle -> vehicle.getY()).average().orElse(Double.NaN);
//                    System.out.println("T:"+world.getTickIndex()+" rotating 1 line around "+x1+"x"+y1);
            delayedMoves.add(move -> {
                move.setAction(ActionType.ROTATE);
                move.setX(x1);
                move.setY(y1);
                move.setAngle(-Math.PI / 4);
            });
            select(0, 90, world.getWidth(), 150);
            delayedMoves.add(move -> {
                move.setAction(ActionType.ASSIGN);
                move.setGroup(20);
            });
            double x2 = streamVehicles(Ownership.ALLY).filter(vehicle -> vehicle.getY() < 150 && vehicle.getY() > 90).mapToDouble(vehicle -> vehicle.getX()).average().orElse(Double.NaN);
            double y2 = streamVehicles(Ownership.ALLY).filter(vehicle -> vehicle.getY() < 150 && vehicle.getY() > 90).mapToDouble(vehicle -> vehicle.getY()).average().orElse(Double.NaN);
//                    System.out.println("T:" + world.getTickIndex() + " rotating 2 line around " + x2 + "x" + y2);
            delayedMoves.add(move -> {
                move.setAction(ActionType.ROTATE);
                move.setX(x2);
                move.setY(y2);
                move.setAngle(-Math.PI / 4);
            });
            select(VehicleType.TANK);
            delayedMoves.add(move -> {
                move.setAction(ActionType.ASSIGN);
                move.setGroup(30);
            });
            double x3 = streamVehicles(Ownership.ALLY).filter(vehicle -> vehicle.getType() == VehicleType.TANK).mapToDouble(vehicle -> vehicle.getX()).average().orElse(Double.NaN);
            double y3 = streamVehicles(Ownership.ALLY).filter(vehicle -> vehicle.getType() == VehicleType.TANK).mapToDouble(vehicle -> vehicle.getY()).average().orElse(Double.NaN);
//                    System.out.println("T:" + world.getTickIndex() + " rotating 3 line around " + x3 + "x" + y3);
            delayedMoves.add(move -> {
                move.setAction(ActionType.ROTATE);
                move.setX(x3);
                move.setY(y3);
                move.setAngle(-Math.PI / 4);
            });
            step = 4;
            tickToNextStep = 490;
        } else if (step == 4) {
            select(VehicleType.TANK);
            moveTo(-25, -25);
            select(VehicleType.ARRV);
            moveTo(75, 75);
            select(20);
            moveTo(25, 25);
            step = 5;
            tickToNextStep = 800;
        } else if (step == 5) {
            select(0, 0, 1024, 1024);
            delayedMoves.add(move -> {
                move.setAction(ActionType.DESELECT);
                move.setVehicleType(VehicleType.FIGHTER);
                move.setRight(world.getWidth());
                move.setBottom(world.getHeight());
            });
            delayedMoves.add(move -> {
                move.setAction(ActionType.ASSIGN);
                move.setGroup(42);
            });
            double x = streamVehicles(Ownership.ALLY).mapToDouble(vehicle -> vehicle.getX()).average().orElse(Double.NaN);
            double y = streamVehicles(Ownership.ALLY).mapToDouble(vehicle -> vehicle.getY()).average().orElse(Double.NaN);
            scale(x, y, 0.1);
            step = 6;
            tickToNextStep = 900;
        } else if (step == 6) {
        	step = 7;
        }
    }

    private void moveHamburger() {

        if (world.getTickIndex() % 500 == 0) {
            double x = streamVehicles(Ownership.ALLY).filter(vehicle -> vehicle.getType() != VehicleType.FIGHTER).mapToDouble(vehicle -> vehicle.getX()).average().orElse(Double.NaN);
            double y = streamVehicles(Ownership.ALLY).filter(vehicle -> vehicle.getType() != VehicleType.FIGHTER).mapToDouble(vehicle -> vehicle.getY()).average().orElse(Double.NaN);
            select(99);
            double xR = streamVehicles(Ownership.ALLY).filter(vehicle -> Arrays.asList(vehicle.getGroups()).contains(99)).mapToDouble(vehicle -> vehicle.getX()).average().orElse(Double.NaN);
            double yR = streamVehicles(Ownership.ALLY).filter(vehicle -> Arrays.asList(vehicle.getGroups()).contains(99)).mapToDouble(vehicle -> vehicle.getY()).average().orElse(Double.NaN);
            moveTo(x - xR, y - yR);
            select(98);
            double xL = streamVehicles(Ownership.ALLY).filter(vehicle -> Arrays.asList(vehicle.getGroups()).contains(98)).mapToDouble(vehicle -> vehicle.getX()).average().orElse(Double.NaN);
            double yL = streamVehicles(Ownership.ALLY).filter(vehicle -> Arrays.asList(vehicle.getGroups()).contains(98)).mapToDouble(vehicle -> vehicle.getY()).average().orElse(Double.NaN);
            moveTo(x - xL, y - yL);
            select(42);
        } else if (world.getTickIndex() % 500 < 100) {

        } else if (world.getTickIndex() % 500 == 100) {
            double x = streamVehicles(Ownership.ALLY).filter(vehicle -> vehicle.getType() != VehicleType.FIGHTER).mapToDouble(vehicle -> vehicle.getX()).average().orElse(Double.NaN);
            double y = streamVehicles(Ownership.ALLY).filter(vehicle -> vehicle.getType() != VehicleType.FIGHTER).mapToDouble(vehicle -> vehicle.getY()).average().orElse(Double.NaN);
            scale(x, y, 0.1);
        } else if (world.getTickIndex() % 500 < 150) {

        } else {
            double x = streamVehicles(Ownership.ALLY).filter(vehicle -> vehicle.getType() != VehicleType.FIGHTER).mapToDouble(vehicle -> vehicle.getX()).average().orElse(Double.NaN);
            double y = streamVehicles(Ownership.ALLY).filter(vehicle -> vehicle.getType() != VehicleType.FIGHTER).mapToDouble(vehicle -> vehicle.getY()).average().orElse(Double.NaN);
            Vehicle closestEnemy = streamVehicles(Ownership.ENEMY).filter(vehicle -> vehicle.getType() != VehicleType.FIGHTER && vehicle.getX() > 32.0 && vehicle.getY() > 32.0 && vehicle.getX() < world.getWidth() - 32.0 && vehicle.getY() < world.getHeight() - 32.0).min((o1, o2) -> {
                double d1 = o1.getDistanceTo(x, y);
                double d2 = o2.getDistanceTo(x, y);
                if (d1 > d2) {
                    return 1;
                } else if (d1 < d2) {
                    return -1;
                } else {
                    return 0;
                }
            }).orElse(null);
            if (closestEnemy != null) {
                if (currentGroup != 42) {
                    select(42);
                }
                calcCurrentHamburgerDirection(typeToCoords.get(VehicleType.ARRV), new Point2D(x, y));

                Vector2D vectorToEnemy = new Vector2D(x, y, closestEnemy.getX(), closestEnemy.getY());
                double dirToEnemy = vectorToEnemy.getAngle();
                if (currentHamburgerDirection < 42 && StrictMath.abs(dirToEnemy - currentHamburgerDirection) > StrictMath.PI / 12 && closestEnemy.getDistanceTo(x,y) > 100) {
                    delayedMoves.add(move -> {
                        move.setAction(ActionType.ROTATE);
                        move.setX(x);
                        move.setY(y);
                        move.setAngle(dirToEnemy - currentHamburgerDirection);
                        move.setMaxSpeed(0.15);
                    });
                } else {
                    double targetX = closestEnemy.getX();
                    double targetY = closestEnemy.getY();
                    if (targetX < 32.0) {
                        targetX = 32.0;
                    } else if (targetX > world.getWidth() - 32.0) {
                        targetX = world.getWidth() - 32.0;
                    }
                    if (targetY < 32.0) {
                        targetY = 32.0;
                    } else if (targetY > world.getHeight() - 32.0) {
                        targetY = world.getHeight() - 32.0;
                    }
                    double distance = (new Point2D(x,y)).getDistanceTo(targetX, targetY);
                    if (me.getScore()-enemy.getScore() > 10 && distance > 200) {
                        moveTo(80D-x,80D-y, 0.2);
                    } else {
                        moveTo(targetX - x, targetY - y, 0.2);
                    } 
                }
            } else {
                moveTo(80D-x,80D-y, 0.2);
			}
        }
    }

    private int fighterScalingToTick = 0;

    private void moveFighters() {
    	Point2D coords = typeToCoords.get(VehicleType.FIGHTER);
        double x = streamVehicles(Ownership.ALLY).filter(vehicle -> vehicle.getType() != VehicleType.FIGHTER).mapToDouble(vehicle -> vehicle.getX()).average().orElse(Double.NaN);
        double y = streamVehicles(Ownership.ALLY).filter(vehicle -> vehicle.getType() != VehicleType.FIGHTER).mapToDouble(vehicle -> vehicle.getY()).average().orElse(Double.NaN);
//                Point2D butterCoords = new Point2D(x,y);
        if (enemy.getNextNuclearStrikeVehicleId() > 0) {
            Vehicle nuclearLighter = unitById.get(enemy.getNextNuclearStrikeVehicleId());
            select(VehicleType.FIGHTER);
            moveTo(nuclearLighter.getX()-x, nuclearLighter.getY()-y);
        } else if (world.getTickIndex() - fighterScalingToTick > 200) {
		    select(VehicleType.FIGHTER);
		    scale(coords.getX(), coords.getY(), 0.1);
		    fighterScalingToTick = world.getTickIndex()+50;
		} else if (world.getTickIndex() >= fighterScalingToTick) {
            Vehicle closestEnemy = streamVehicles(Ownership.ENEMY).filter(vehicle -> vehicle.getType() == VehicleType.FIGHTER || vehicle.getType() == VehicleType.HELICOPTER).min((Vehicle o1, Vehicle o2) -> {
                double d1 = o1.getDistanceTo(x, y);
                double d2 = o2.getDistanceTo(x, y);
                if (d1 > d2) {
                    return 1;
                } else if (d1 < d2) {
                    return -1;
                } else {
                    return 0;
                }
            }).orElse(null);
            if (closestEnemy == null) {
                fighterNeeds = false;
                select(VehicleType.FIGHTER);
                moveTo(300, 1024);
            } else {
                Point2D target = null;
                boolean health = streamVehicles(Ownership.ALLY, VehicleType.FIGHTER).anyMatch(vehicle -> vehicle.getDurability() < vehicle.getMaxDurability());
                if (closestEnemy.getDistanceTo(x,y) < 100 && target.getDistanceTo(coords) > 30) {
                    target = new Point2D(closestEnemy);
                } else if (closestEnemy.getDistanceTo(x,y) < 500) {
                    Vector2D vectorFromEnemy = new Vector2D(closestEnemy.getX(), closestEnemy.getY(), x, y);
                    vectorFromEnemy.setLength(20.0D);
                    target = new Point2D(closestEnemy);
                    target.add(vectorFromEnemy);
                } else if (health) {
                	Point2D rCoords = typeToCoords.get(VehicleType.ARRV);
                	Point2D hCoords = typeToCoords.get(VehicleType.HELICOPTER);
                	if (rCoords != null) {
                		if (hCoords == null || coords.getDistanceTo(rCoords) < hCoords.getDistanceTo(rCoords)) {
                			target = rCoords;
                		} else {
                			Vector2D middlePoint = new Vector2D(coords, rCoords);
                			middlePoint.multiply(0.5);
                			Vector2D normal = new Vector2D(hCoords.getX(), hCoords.getY(), middlePoint.getX(), middlePoint.getY());
                			normal.setLength(100);
                			target = new Point2D(middlePoint.getX(), middlePoint.getY());
                			target.add(normal);
                		}
                	} else {
	                    Vector2D vectorToEnemy = new Vector2D(x,y,closestEnemy.getX(), closestEnemy.getY());
	                    vectorToEnemy.setLength(150.0D);
	                    target = new Point2D(x+vectorToEnemy.getX(), y+vectorToEnemy.getY());
                	}
                } else {
                    Vector2D vectorToEnemy = new Vector2D(x,y,closestEnemy.getX(), closestEnemy.getY());
                    vectorToEnemy.setLength(300.0D);
                    target = new Point2D(x+vectorToEnemy.getX(), y+vectorToEnemy.getY());
                }
                select(VehicleType.FIGHTER);
                moveTo(target.getX() - coords.getX(), target.getY() - coords.getY());
            }
        }
    }

    private void move() {


        if (world.getTickIndex() == 0) {
        	tickZero();
        	return;
        }
        if (!allPlaced) {
        	placeAmebes();
        	return;
        }

        if ((fighterNeeds && workTick % 10 == 9) || enemy.getNextNuclearStrikeVehicleId() > 0) {
        	moveFighters();
        }

        if (step < 7) {
        	buildHamburger();
        	return;
        }
		if (world.getTickIndex() % 10 == 0) {
			moveHamburger();
			return;
        }

    }

    private void calcCurrentHamburgerDirection(Point2D lastLine, Point2D middleLine) {
        if (lastLine != null) {
            Vector2D vector = new Vector2D(lastLine, middleLine);
            currentHamburgerDirection = vector.getAngle();
        } else {
            currentHamburgerDirection = 42;
        }
    }
    
    private int currentGroup = 0;

    private void select(double left, double top, double right, double bottom, VehicleType type, int group) {
        delayedMoves.add(move -> {
            move.setAction(ActionType.CLEAR_AND_SELECT);
            move.setTop(top);
            move.setLeft(left);
            move.setBottom(bottom);
            move.setRight(right);
            move.setVehicleType(type);
            move.setGroup(group);
        });
        currentGroup = group;
    }

    private void select(double x1, double y1, double x2, double y2, VehicleType type) {
        select(x1, y1, x2, y2, type, 0);
    }

    private void select(double left, double top, double right, double bottom) {
        select(left, top, right, bottom, null, 0);
    }

    private void select(VehicleType type) {
        select(0, 0, world.getWidth(), world.getHeight(), type, 0);
    }

    private void select(int group) {
        select(0, 0, world.getWidth(), world.getHeight(), null, group);
    }

    private void moveTo(double x, double y) {
        moveTo(x, y, 0.0);
    }

    private void moveTo(double x, double y, double speed) {
        delayedMoves.add(move -> {
            move.setAction(ActionType.MOVE);
            move.setX(x);
            move.setY(y);
            move.setMaxSpeed(speed);
        });
    }

    private void scale(double x, double y, double factor) {
        delayedMoves.add(move -> {
            move.setAction(ActionType.SCALE);
            move.setX(x);
            move.setY(y);
            move.setFactor(factor);
        });
    }

}
