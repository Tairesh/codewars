
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;
import model.*;


/**
 *
 * @author ilya
 */
public class WorldObserver {
    
    private static WorldObserver instance = null;
    
    private WorldObserver() {
        
    }
    
    public static WorldObserver instance() {
        if (instance == null) {
            instance = new WorldObserver();
        }
        return instance;
    }
    
    public void observe(MyStrategy s) {
        
        // обновляем инфо о машинках
        
        for (Vehicle vehicle : s.world.getNewVehicles()) {
            s.unitById.put(vehicle.getId(), vehicle);
            s.updateTickByUnitId.put(vehicle.getId(), s.world.getTickIndex()-1);
        }

        for (VehicleUpdate vehicleUpdate : s.world.getVehicleUpdates()) {
            long vehicleId = vehicleUpdate.getId();

            if (vehicleUpdate.getDurability() == 0) {
                s.unitById.remove(vehicleId);
                s.updateTickByUnitId.remove(vehicleId);
            } else {
                if (vehicleUpdate.getX() != s.unitById.get(vehicleId).getX() || vehicleUpdate.getY() != s.unitById.get(vehicleId).getY()) {
                    s.updateTickByUnitId.put(vehicleId, s.world.getTickIndex());
                }
                s.unitById.put(vehicleId, new Vehicle(s.unitById.get(vehicleId), vehicleUpdate));
            }
        }
        
        // строим группы
        
//        List<Group> unitGroups = new ArrayList<>(20);
//        
//        for (Vehicle unit : s.unitById.values()) {
//            int addedToGroup = -1;
//            for (int i : IntStream.range(0, unitGroups.size()).toArray()) {
//                Group group = unitGroups.get(i);
//                if (group.deleted || group.playerId != unit.getPlayerId()) {
//                    continue;
//                }
//                if (group.unitsById.values().stream().anyMatch(gu -> gu.getDistanceTo(unit) < 10.0)) {
//                    // юнит близко к этой группе
//                    group.add(unit);
//                    
//                    // он уже был добавлен в другую, так что объединяем их
//                    if (addedToGroup > 0) {
//                        unitGroups.get(addedToGroup).addAll(group.unitsById);
//                        group.delete();
//                    }
//                    addedToGroup = i;
//                }
//            }
//            
//            if (addedToGroup == -1) {
//                // создаём новую группу для юнита
//                Group group = new Group(unit.getPlayerId());
//                group.add(unit);
//                unitGroups.add(group);
//            }
//        }
//        
//        s.unitGroups.clear();
//        unitGroups.stream().filter((group) -> (!group.deleted)).forEach((group) -> {
//            s.unitGroups.add(group);
//        });

//        s.rush = s.streamVehicles(MyStrategy.Ownership.ENEMY, VehicleType.FIGHTER).anyMatch(unit -> unit.getX() < 800 && unit.getY() < 800);
        
    }
    
}
